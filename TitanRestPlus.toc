## Interface: 70300
## Title: Titan Panel [|cffeda55fRest+|r] |cff00aa007.3.5.0|r
## Notes: Keeps track of the RestXP amounts and status for all of your characters. Is it updated to run on patch 7.x.
## Author: Madysen
## Author: GrayElf, Maillen, Vogonjeltz, Kernighan
## DefaultState: Enabled
## SavedVariables: RestPlus_Data, RestPlus_Settings, RestPlus_Colors
## OptionalDeps:
## Dependencies: Titan
## Version: 5.12.6.70300
## X-Category: Interface Enhancements
## X-Date: 2017-9-9
## X-Curse-Packaged-Version: 5.12.6.70300
## X-Curse-Project-Name: Titan Panel [RestPlus]
## X-Curse-Project-ID: rest_plus
## X-Curse-Repository-ID: wow/rest_plus/mainline
TitanRestPlus.xml